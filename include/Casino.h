#ifndef CASINO_GAME_H_INCLUDED
#define CASINO_GAME_H_INCLUDED

#include <PileOuFace/Game.h>
#include <Mastermind/Game.h>
#include <Roulette/Game.h>

namespace Casino
{

class Game
{
public:
    Game();
    ~Game();

    void Launch();

private:
    Mastermind::Game mastermind;
    Roulette::Game roulette;
    PileOuFace::Game pileOuFace;
};

}

#endif // GAME_H_INCLUDED

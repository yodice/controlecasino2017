#ifndef CASINO_IO_H_INCLUDED
#define CASINO_IO_H_INCLUDED

#include <iostream>
#include <string>
#include <map>

#include <Joueur.h>

namespace Casino
{

// J'aurais pu utiliser un namespace mais il est
// sp�cifi� qu'il peut exister que 0 fonctions ...
class IO
{
public:
    // Asks yes or no and iterates while answer is not what
    // is expected
    static bool askYesOrNo(char yes, char no, std::string question);

    // Asks the user to choose between a list of choices
    // typed with for each the signification for the user
    // of the input. Iterates while choice is not right.
    static char askToChooseBetween(
                            std::map< char, std::string > choices,
                            std::string question);

    // Asks the user for a bet. If the chosen number of token
    // is greater than maxNbTokens it returns 0.
    static int askBet(int maxNbTokens);

    // Display a Joueur class instance
    static void displayJoueur(Joueur &);

    // Ask a name for a Joueur instance
    static std::string askJoueurName();
};


}

#endif // IO_H_INCLUDED

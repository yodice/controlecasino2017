#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED

#define PLAY_MASTERMIND 0
#define PLAY_ROULETTE 1
#define PLAY_PILEOUFACE 2

#include <map>

namespace Casino
{

class Joueur
{
public:
    Joueur();
    ~Joueur();

    void setName(std::string new_name)
    { this->name = new_name; }

    std::string getName()
    { return this->name; }

    static int chooseGame();
    void chooseBet();

    void addTokens(int n)
    { this->tokens += n; }

    void subTokens(int n)
    { this->tokens -= n; }

    int getTokens()
    { return this->tokens; }

    int getBet()
    { return this->bet; }

private:
    int tokens;
    int bet;

    std::string name;
};

}

#endif // JOUEUR_H_INCLUDED

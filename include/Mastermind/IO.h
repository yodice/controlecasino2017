#ifndef MASTERMIND_IO_H_INCLUDED
#define MASTERMIND_IO_H_INCLUDED

#include <Mastermind/Game.h>

namespace Mastermind
{

class IO
{
public:
    static void askSecret(uint secret[4]);
    static void displayHint(Hint hint[4]);
    static void displayWinsAndLoses(uint won_games, uint loosed_games);
};

}

#endif // IO_H_INCLUDED

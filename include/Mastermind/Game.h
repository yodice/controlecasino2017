#ifndef MASTERMIND_GAME_H_INCLUDED
#define MASTERMIND_GAME_H_INCLUDED

#define REMAINING_TRIES 10

namespace Mastermind
{

typedef unsigned int uint;

enum Hint { Misplaced, Placed, Non_existant };

class Game
{
public:
    Game();
    ~Game();

    bool Launch();

private:
    void generateSecret();
    void flushGuessedSct();
    bool mainLoop();
    bool secretCmp();

    void init();

    enum Hint hint[4];

    uint secret[4];
    uint guessed_secret[4];
    uint remaining_tries;
    uint loosed_games;
    uint won_games;
};

}

#endif // GAME_H_INCLUDED

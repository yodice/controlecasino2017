#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

namespace Roulette
{

class Player
{
public:
    Player(); // Future initialization function
    ~Player();

    int getTokens()
    { return this->tokens; }

    void setTokens(int new_tokens)
    { this->tokens = new_tokens; }

    void Kill()
    { this->isLiving = false; }

    bool isAlive()
    { return (isLiving) ? true : false; }

private:
    int tokens;
    bool isLiving;
};

}

#endif // PLAYER_H_INCLUDED

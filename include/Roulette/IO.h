#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

/*          Strings that are used in the game            */

#define MSG_RRBEGIN "\
=================\n\
  OUT OF TOKENS \n\
=================\n"

#define USRERR_WRONG_BET    "You cannot bet higher than 25 or lower than 1, your bet must not be higher than your actual tokens"
#define USRERR_WRONG_PARITY "Please enter 0 for even and 1 for odd"

#define MSG_USR_EARNS    "You earn"
#define MSG_USR_LOOSES   "You loose"
#define MSG_USR_WINNING0 "No token earned"

#define MSG_TOKS_GE50_0 "It looks that there is a way to finish this alive ..."    // User tokens greater or equal than 50
#define MSG_TOKS_GE50_1 "You feel like you will go back to this casino"

#define MSG_TOKS_L50_0 "You feel drips of perspiration beading up your temples ..." // Lower than 50
#define MSG_TOKS_L50_1 "You feel a deep fear thrill meaning that you are closer from the end ..."

#include "Player.h"

namespace Roulette
{

class IO
{
public:
    static int chooseBet(int maxNbTokens);
    static void onTurnComplete(int winnedTokens);
    static void onExit(Player *);
    static void dispPlayer(Player *);
};


}

#endif // DISPLAY_H_INCLUDED

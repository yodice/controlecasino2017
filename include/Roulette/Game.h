#ifndef ROULETTE_GAME_H_INCLUDED
#define ROULETTE_GAME_H_INCLUDED

#include <Roulette/Player.h>
#include <Roulette/IO.h>

namespace Roulette
{

class Game
{
public:
    // [!] Probl�me � la compilation:
    // refuse de prendre en compte les changements
    // dans le corps de Launch() donc j'ai gard�
    // le nom mainLoop() pour ne faire qu'un seul tour de jeu
    bool Launch() { return this->mainLoop(); }

private:
    // Play one turn of the game. Returns true if user has won.
    // false if else.
    bool mainLoop();
    bool doRoulette();
};

}

#endif // GAME_H_INCLUDED

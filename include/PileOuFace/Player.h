#ifndef PLAYER_H
#define PLAYER_H

#include <string>

namespace PileOuFace
{

enum PlayerType { GAMER, COMPUTER };

class Player
{
    public:
        Player();
        virtual ~Player();

        unsigned int GetWinnedCounter() { return m_WinnedCounter; }
        void SetWinnedCounter(unsigned int val) { m_WinnedCounter = val; }

        std::string GetName() { return this->m_Name; }
        void SetName(const char * a_name) { m_Name = a_name; }

        bool DigUp();

        void Win();

        bool HasWon();

    protected:

    private:
        std::string m_Name;
        unsigned int m_WinnedCounter;
};

}

#endif // PLAYER_H

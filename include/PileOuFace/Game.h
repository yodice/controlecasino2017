#ifndef GAME_H
#define GAME_H

#include "Player.h"

#include <cstdlib>
#include <ctime>
#include <chrono>

namespace PileOuFace
{

class Game
{
    public:
        Game();
        virtual ~Game();

        bool Launch();
        void Turn(Player *a_p1, Player *a_p2);

    protected:

    private:
        bool GameLoop();
        bool ChooseFace(enum PlayerType);
};

}

#endif // GAME_H

#include <IO.h>

#include <Casino.h>

using namespace std;

int main()
{
    Casino::Game casino;

    casino.Launch();

    return EXIT_SUCCESS;
}

#include <Joueur.h>
#include <IO.h>

namespace Casino
{

Joueur::Joueur()
{
    this->tokens = 10;
    this->bet = 0;
}

Joueur::~Joueur()
{}

void Joueur::chooseBet()
{
    do { this->bet = IO::askBet(this->tokens); }
    while(this->bet == 0);
}

int Joueur::chooseGame()
{
    int returnedValue = -1;
    std::map< char, std::string > choices;

    choices.insert( std::pair< char, std::string >('p', "Pile ou face"));
    choices.insert( std::pair< char, std::string >('m', "Mastermind"));
    choices.insert( std::pair< char, std::string >('r', "Roulette"));

    switch(IO::askToChooseBetween(choices,
                                "Jeu auquel vous allez jouer"))
    {
        case 'p':
            returnedValue = PLAY_PILEOUFACE;
            break;
        case 'm':
            returnedValue = PLAY_MASTERMIND;
            break;
        case 'r':
            returnedValue = PLAY_ROULETTE;
            break;
    }

    return returnedValue;
}

}

#include <Mastermind/IO.h>

#include <iostream>

namespace Mastermind
{

void IO::askSecret(uint sct[4])
{
    std::cout << "Enter four numbers"
        << " (separated by any space character) :"
        << std::endl;

    for(uint i=0; i < 4; i++)
    {
        std::cin >> sct[ i ];
        if(sct[ i ] > 4 || sct[ i ] < 1)
        {
            std::cout
                << "Wrong number [" << i+1 << ']'
                << std::endl
                << "Must be between 1 and 4"
                << std::endl
                << " Enter numbers again"
                << std::endl;

            continue;
        }
    }
}

void IO::displayHint(Hint hint[4])
{
 /*
 * m for Misplaced
 * p for Placed
 * n for Nothing
 */

    for(uint i=0; i < 4; i++)
    {
        switch(hint[ i ])
        {
            case Placed:
                std::cout << "p\t";
                break;

            case Misplaced:
                std::cout << "m\t";
                break;

            default:
                std::cout << "n\t";
        }
    }
    std::cout << std::endl;
}

void IO::displayWinsAndLoses(uint won_games, uint loosed_games)
{
    std::cout << std::endl << "You won : " << won_games
        << " times" << std::endl << "You loosed : "
        << loosed_games << " times" << std::endl;
}

}

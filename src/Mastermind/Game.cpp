#include <Mastermind/Game.h>

#include <cstdlib>

#include <IO.h>
#include <Mastermind/IO.h>

namespace Mastermind
{

Game::Game()
{
    this->init();
}

Game::~Game()
{
}

bool Game::Launch()
{
    this->init();
    return this->mainLoop();
}

void Game::init()
{
/*
 * Generates the secret, the number of
 * tries and the won and loosed games.
 * It also instantiate each element of
 * the "hint" array to Non-existant.
 */

    this->remaining_tries = REMAINING_TRIES;
    this->loosed_games = 0, this->won_games = 0;
}

// Randomly generate secret
void Game::generateSecret()
{
    for(uint i=0; i < 4; i++)
        this->secret[ i ] = (std::rand() % 4) + 1;
}

// Fills of a null value each case of the guessed secret
void Game::flushGuessedSct()
{
    for(uint i=0; i < 4; i++)
        this->hint[ i ] = Non_existant;
}

// This is not a real loop but I kept that name
// because I feel lazy
bool Game::mainLoop()
{
    //bool userQuits = false;
    bool userHasWon = false;

    // I just want to play one turn of the game !
    /*while(!userQuits)
    {*/

    this->generateSecret();
    this->flushGuessedSct();

    this->remaining_tries = REMAINING_TRIES;

    userHasWon = false;

    while(this->remaining_tries > 0
            && !userHasWon)
    {
        IO::askSecret(this->guessed_secret);
        userHasWon = this->secretCmp();

        if(!userHasWon)
            IO::displayHint(this->hint);

        this->remaining_tries = this->remaining_tries - 1;
    }

    if(userHasWon)
        this->won_games = this->won_games + 1;
    else
        this->loosed_games = this->loosed_games + 1;

    IO::displayWinsAndLoses(this->won_games,
                            this->loosed_games);

        /*userQuits = Casino::IO::askYesOrNo('y','n',
                                            "Wanna play again ?");

        // Value of boolean is inverted due to the
        // loop condition
        userQuits = !userQuits;*/
    //}

    return userHasWon;
}

// Compare guessed and real secret.
// Fills the Game.hint array with the right values
bool Game::secretCmp()
{
    bool found = false;

    for(uint i=0; i < 4; i++)
    {
        for(uint j=0; j < 4; j++)
        {
            if(this->guessed_secret[ i ] == this->secret[ j ])
            {
                if(i == j)
                {
                    this->hint[ i ] = Placed;
                    break;
                }
                else
                {
                    this->hint[ i ] = Misplaced;
                    found = true;
                }
            } else if(!found)
                this->hint[ i ] = Non_existant;
        }
    }

    uint i;     /* I need to keep it in order to check
                 * if the user has won or not once the
                 * check has been done
                 */

    for(i=0 ; i < 4; i++)
    {
        if(this->hint[ i ] == Misplaced
                || this->hint[ i ] == Non_existant)
        {
            break;
        }
    }

    if(i >= 3)
        return true;

    return false;
}

}

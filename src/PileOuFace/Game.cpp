#include <PileOuFace/Game.h>

#include <iostream>

namespace PileOuFace
{

Game::Game()
{
    srand(time(NULL));
}

bool Game::Launch()
{
    return this->GameLoop();
}

bool Game::ChooseFace(enum PlayerType playerType)
{
    bool m_choice;

    if(playerType == GAMER)
    {
        std::cout << "Choisissez pile ou face (0 => pile, 1 => face) : " << std::endl;
        std::cin >> m_choice;
    } else
    {
        m_choice = rand() % 2;

        if(m_choice == true)
            std::cout << "Votre concurrent a choisi face" << std::endl;
        else
            std::cout << "Votre concurrent a choisi pile" << std::endl;
    }

    return m_choice;
}

// If returns true player has won
bool Game::GameLoop()
{
    Player l_p1, l_p2;
    bool playerHasWon;

    l_p1.SetName("Joueur");
    l_p2.SetName("Abel");

    while( !l_p1.HasWon() && !l_p2.HasWon() )
    {
        this->Turn(&l_p1, &l_p2);
    }

    if(l_p1.HasWon())
    {
        std::cout << l_p1.GetName() << " a gagne" << std::endl;
        playerHasWon = true;
    }
    else if(l_p2.HasWon())
    {
        std::cout << l_p2.GetName() << " a gagne" << std::endl;
        playerHasWon = false;
    }

    return playerHasWon;
}

void Game::Turn(Player *a_p1, Player *a_p2)
{
    static int l_turnsCounter = 1;

    bool l_winValue;

    if(l_turnsCounter % 2 > 0)
        l_winValue = this->ChooseFace(GAMER);
    else
        l_winValue = this->ChooseFace(COMPUTER);

    if(a_p1->DigUp() == l_winValue)
        a_p1->Win();

    if(a_p2->DigUp() == l_winValue)
        a_p2->Win();

    l_turnsCounter = l_turnsCounter + 1;
}

Game::~Game()
{
    //dtor
}

}

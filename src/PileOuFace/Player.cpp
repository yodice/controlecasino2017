#include <PileOuFace/Player.h>

#include <random>
#include <iostream>

namespace PileOuFace
{

Player::Player()
{
    m_WinnedCounter = 0;
    m_Name = "Joueur";
}

void Player::Win()
{
    m_WinnedCounter++;
    std::cout << m_Name << " a gagn� 1 point !" << std::endl;
}

bool Player::DigUp()
{
    bool l_randomChoice;

    if((rand() % 2) > 0)
        l_randomChoice = true;
    else
        l_randomChoice = false;

    if(l_randomChoice == true)
        std::cout << m_Name << " est tomb� sur face" << std::endl;
    else
        std::cout << m_Name << " est tomb� sur pile" << std::endl;

    return l_randomChoice;
}

bool Player::HasWon()
{
    if(m_WinnedCounter >= 3)
        return true;
    else return false;
}

Player::~Player()
{
}

}

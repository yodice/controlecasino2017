#include <Casino.h>
#include <Joueur.h>

#include <IO.h>

// Puisque je donne le m�me nom de classe
// � tous mes jeux j'emp�che les conflits
// de noms avec un namespace portant le nom
// du jeu � chaque fois
namespace Casino
{

Game::Game()
{
}

Game::~Game()
{
}

void Game::Launch()
{
    bool playerHasWon;
    Joueur player;

    player.setName(IO::askJoueurName());
    IO::displayJoueur(player);

    do
    {

        player.chooseBet();
        switch(Joueur::chooseGame())
        {
            case PLAY_MASTERMIND:
                playerHasWon = this->mastermind.Launch();
                break;
            case PLAY_ROULETTE:
                playerHasWon = this->roulette.Launch();
                break;
            case PLAY_PILEOUFACE:
                playerHasWon = this->pileOuFace.Launch();
                break;

            default:
                return;
        }

        if(playerHasWon)
            player.addTokens(player.getBet() * 2);
        else
            player.subTokens(player.getBet());

        IO::displayJoueur(player);

        if(player.getTokens() < 1)
            return;

    } while(IO::askYesOrNo('y', 'n', "Wanna play another game ?"));
}

}

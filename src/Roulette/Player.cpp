#include <Roulette/Player.h>

namespace Roulette
{

Player::Player() // Lazy programming : no malloc() -> no free() !
{
    this->tokens = 10;
    this->isLiving = true;
}

Player::~Player()
{
}

}

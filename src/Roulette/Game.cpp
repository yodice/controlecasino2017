#include <Roulette/Game.h>

#include <iostream>
#include <cstdlib>

#define _DEBUG

#define WINNING_SCORE 100
#define ROULETTE_NBS 36

#include <IO.h>

/* There is no imbricated loops in this code, forgetting user input exceptions.
 * Static variables are used in doRussianRoulette() and booleans set its behavior
 */
namespace Roulette
{

bool Game::mainLoop()
{
    Player p;

    enum GameChoice { NormalRoulette, RussianRoulette }
        gameChoice = NormalRoulette;

    bool hasWon = false;

    if(gameChoice == NormalRoulette)
    {
        return doRoulette();
    }

    IO::onExit(&p);

    return hasWon;
}

bool Game::doRoulette()
{
    int rouletteNumber;
    int chosenParity=-1;

    while(chosenParity < 0 || chosenParity > 1)
    {
        std::cout << "Even or odd ? (0/1):";
        std::cin >> chosenParity;
        if(chosenParity < 0 || chosenParity > 1)
            std::cout << USRERR_WRONG_PARITY << std::endl;
    }

    rouletteNumber = std::rand() % (ROULETTE_NBS + 1);

    std::cout << "Number is " << rouletteNumber << std::endl;

    if(chosenParity == rouletteNumber % 2)
        return true;
    else
        return false;

    return false;
}

}

#include <Roulette/IO.h>
#include <Roulette/Player.h>

#include <iostream>
#include <cstdlib>
#include <cmath>

namespace Roulette
{

int IO::chooseBet(int maxNbTokens)
{
    int retBet;

    std::cout << "Enter your bet:";
    std::cin >> retBet;

    // If user input is wrong
    if( (retBet < 1) || (retBet > 25) ||
       (retBet > maxNbTokens))
    {
        return 0;
    }

    return retBet;
}

// Multiple messages are available and chosen from a random factor
void IO::dispPlayer(Player *p)
{
    std::string displayed;
    int strToDisplay = std::rand() % 2;

    std::cout << std::endl << "Tokens:" << p->getTokens() << std::endl;

    if(p->getTokens() >= 50)
    {
        switch(strToDisplay)
        {
            case 0:
                displayed = MSG_TOKS_GE50_0;
                break;
            default:
                displayed = MSG_TOKS_GE50_1;
        }
    }
    else
    {
        switch(strToDisplay)
        {
            case 0:
                displayed = MSG_TOKS_L50_0;
                break;
            default:
                displayed = MSG_TOKS_L50_1;
        }
    }

    std::cout << displayed << std::endl << std::endl;
}

void IO::onTurnComplete(int winnedTokens)
{
    if(winnedTokens > 0)
        std::cout << MSG_USR_EARNS;
    else if(winnedTokens < 0)
        std::cout << MSG_USR_LOOSES;

#ifdef _DEBUG
    else
    {
        std::cout << "Error :" << MSG_ERR_WINNING0;  << std::endl;
        return;
    }
#endif
    std::cout << ' ' << std::abs(winnedTokens) << " tokens" << std::endl;
}

void IO::onExit(Player *p)
{
     // If player is still alive and ends the loop he
     // won by his number of tokens ...
    if(!p->isAlive())
        std::cout << "You died" << std::endl;
    else if(p->isAlive())
        std::cout <<
        "You feel lucky and embrace the sky for your saved life"
        << std::endl;
}

}

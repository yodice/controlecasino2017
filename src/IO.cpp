#include <io.h>

namespace Casino
{

int IO::askBet(int maxNbTokens)
{
    int retBet;

    std::cout << "Entrez votre mise: ";
    std::cin >> retBet;

    // If user input is wrong
    if( (retBet < 1) || (retBet > maxNbTokens) )
    {
        return 0;
    }

    return retBet;
}

bool IO::askYesOrNo(char yes, char no, std::string question)
{
    std::map< char, std::string > choices;

    choices.insert( std::pair<char, std::string>(yes, "Yes") );
    choices.insert( std::pair<char, std::string>(no, "No") );

    const char choice = IO::askToChooseBetween(choices, question);

    if(choice == 'y')
        return true;
    else
        return false;
}

char IO::askToChooseBetween(std::map< char, std::string > choices,
                            std::string question)
{
    char choice;

    std::cout << question << std::endl;

    for(auto it = choices.begin(); it != choices.end(); it++)
    {
        std::cout << it->first << " -> " << it->second
            << std::endl;
    }

    std::cout << "Quel est votre choix ? " << std::endl;

    std::cin >> choice;

    bool is_input_right = false;

    for(auto it = choices.begin(); it != choices.end(); it++)
    {
        if(it->first == choice)
        {
            is_input_right = true;
            break;
        }
    }

    if(!is_input_right)
        return askToChooseBetween(choices, question);
    else { return choice; }
}

void IO::displayJoueur(Joueur &p)
{
    std::cout << p.getName() << "\t-\t"
        << "Jetons restants: " << p.getTokens()
    << std::endl;
}

std::string IO::askJoueurName()
{
    std::string name;

    std::cout << "Nom de votre joueur: ";
    std::cin >> name;

    return name;
}

}
